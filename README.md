# Overview

For this exercise, you'll be given access to our sandbox API and asked to develop
some front-end functionality.  This will be similar to what you'd be doing here
on a day-to-day basis.  You'll be working with one of our other developers and 
are encouraged to ask questions and utilize any docs you would normally use.  
The goal isn't to finish the exercise; the goal is to get feel for working 
together, so relax and have some fun programming!

# Setup

`git clone git@bitbucket.org:idtech/web-interview.git`

Open up localhost:1338 in the browser to access the page.  
There should be an empty google map to start with.

## Installing npm and node.

There is a local proxy to circumvent CORS issues.  You'll make requests to 'localhost:1338' and they'll be proxied to our sandbox API.  

You'll need npm and node installed to run the proxy server.  
You'll can install npm and node through your package manager, or use nvm.

To install with NVM:

* `curl https://raw.githubusercontent.com/creationix/nvm/v0.16.1/install.sh | sh`
* `source ~/.profile`
* `nvm install v0.12.7`
* `nvm alias default v0.12.7`

Once you have node installed:

* `cd proxy`
* `npm install`
* `node index.js`

## To Authenticate

POST to /sessions
```
{   
  "email": "candidate@identifiedtech.com"
  "password": "b7zNr4aQ"
}
```

# API Doc

API supports standard REST operations on models

Action       |   Method   | uri
------------ | ---------- | ----------
Find         |   GET      |   /people/123
Find All     |   GET      |   /people
Update       |   PUT      |   /people/123
Create       |   POST     |   /people
Delete       |   DELETE   |   /people/123

Responses are returned in a (mostly) ember-friendly way.  The following is from Ember-docs.  

The primary record being returned should be in a named root. For example, if you request a record from /people/123, the response should be nested inside a property called person

```
{
  "person": {
    "firstName": "Jeff",
    "lastName": "Atwood"
  }
}
```

To reduce the number of HTTP requests necessary, you can sideload additional records in your JSON response. Sideloaded records live outside the JSON root, and are represented as an array of hashes:

```
{
  "post": {
    "id": 1,
    "title": "Node is not omakase",
    "comments": [1, 2, 3]
  },

  "comments": [{
    "id": 1,
    "body": "But is it _lightweight_ omakase?"
  },
  {
    "id": 2,
    "body": "I for one welcome our new omakase overlords"
  },
  {
    "id": 3,
    "body": "Put me on the fast track to a delicious dinner"
  }]
}
```