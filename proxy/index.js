/*
  local proxy to get around CORS issues.
*/
var http = require('http'),
    request = require('request'),
    fs = require('fs'),
    api = 'http://sandbox.identifiedtech.com:1337/v1';

http.createServer(function (req, res) {

  var endpoint = api + String(req.url).replace('/api', '');

  console.log(req.method, endpoint);

  var opts = {
    url: endpoint,
    headers: req.headers,
    rejectUnauthorized: false
  };

  if (req.url === '/' || req.url === '/index.html') {

    fs.readFile("../index.html", function(err, data){
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write(data);
      res.end();
    });

  } else if (String(req.method).toUpperCase() === 'POST' || String(req.method).toUpperCase() === 'PUT') {

    opts.method = req.method;
    var body = '';
    req.on('data', function (data) {
      body += data;

      if (body.length > Math.pow(5, 10)) {
        req.connection.destroy();
        console.log('destroying a too-large connection: ' + body.length);
      }
    });
    req.on('end', function () {

      opts.body = body;

      request(opts, function (error, response, body) {

        if (response && response.statusCode && response.headers) {
            res.writeHead(response.statusCode, response.headers);
            console.dir(response.statusCode);
            console.dir(response.headers);
        }

        var rBody = 'No response body.';
        if (response && response.body) {
          rBody = response.body;
        }

        if (error) {
          console.dir(error);
          res.end(error);
        } else {
          res.end(rBody);
        }

      });

    });

  } else {

    request(opts, function (error, response, body) {

      if (error) {
        console.log('error');
        console.dir(opts);
        if (response) {
          console.dir(response.statusCode);
          console.dir(response.headers);
          res.writeHead(response.statusCode, response.headers);
        } else {
          res.writeHead(500);
          console.dir("no response object");
        }
        console.dir(JSON.stringify(error));
        res.end(JSON.stringify(error));
      } else {
        console.dir(response.statusCode);
        console.dir(response.headers);
        res.writeHead(response.statusCode, response.headers);
        res.end(body);
      }
    });
  }

}).listen(1338, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1338/');
